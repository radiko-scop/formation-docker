.. include:: <isotech.txt>
.. highlight:: python

.. Pour la présentation, https://obsproject.com/wiki/install-instructions#linux peut servir.

=======================================================================
Introduction aux conteneurs
=======================================================================

Introduction aux conteneurs

.. TOC
.. =======

.. .. contents::

Introduction
========================

Présentation
-------------

- Benjamin Forest
- Radiko
- benjamin.forest@radiko.fr
- Ces slides sont disponibles sur gitlab : https://gitlab.com/radiko-scop/formation-docker

.. Présentation de moi et Radiko. Expériences autour des conteneurs.

Au programme
-------------

- 4 TPs pour apprendre les bases (réutilisés - sous license MIT).
- Une vue plus générale des applications sans rentrer dans le détail.
- (Si trop de temps) Un mini projet.

Livrable & Notation
--------------------

- Notes individuelles.
- Livrable : 1 zip par **personne**:
    * Compte rendu des TPs (simple). *Au format PDF*.
    * Code source produit
- à envoyer à benjamin.forest@radiko.fr
- Avant le 10 mai 2024

À propos des LLMs
--------------------

Il est probable que ChatGPT et compagnie effectuent ce TP mieux que vous.
Leur utilisation doit donc être intelligente.

- Écologie
- Comprehension
- Limites

Virtualisation dans votre entreprise ?
=========================================

- Containers
- qemu
- Steam
- VMs


Une bonne introduction
================================

Le cours de Valentin Brosseau (modifié):

http://localhost:4173/cours/docker.html
ou
https://cours.brosseau.ovh/cours/docker.html

(license MIT)

Devcontainers
================================

Un container partagé entre dev pour développer.

Break - TP4

https://containers.dev/
https://code.visualstudio.com/docs/devcontainers/containers

Docker partout ?
====================

Lire l'article de Bearstech - échange sur le sujet.
https://bearstech.com/societe/blog/docker-en-production-le-cas-dusage-de-bearstech/


.. Docker versus le reste du monde
.. =================================

.. https://pagertree.com/learn/docker/overview


Mini Projet ouvert pour finir
=================================

En utilisant ce que vous avez appris, essayez de mettre en place un serveur de jeu vidéo grâce à docker compose.
Genre https://hub.docker.com/r/lianshufeng/cloud-game ?


Bibliographie
==================

- Bonne vue d'ensemble: https://pagertree.com/learn/docker/overview
- Pas testé finalement: https://www.docker.com/101-tutorial/
- Pas utilisé: https://github.com/microsoft/docker
- pas utilisé: https://github.com/adrianhajdin/docker-course
- Awesome: https://github.com/veggiemonk/awesome-docker
- Katas intéressants utilisé pour les TPs: https://github.com/eficode-academy/docker-katas/blob/master/labs/00-getting-started.md
- En francais, cours variés (license MIT): https://cours.brosseau.ovh
