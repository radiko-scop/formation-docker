# Mini projet


Par groupe de 2-3 max

En utilisant ce qu'on a vu lors des TPs, proposer une image permettant de jouer en ligne au jeu suivant : supertuxkart.

Au moins deux instance du container lancées en paralèe et permettront de jouer en ligne, via un serveur.

On pourra utiliser webtops pour démarrer les clients supertuxkart.

- Le serveur

Livrables attendus:

- Le docker file permettant de lancer le jeu.
- Un fichier docker compose permettant de lancer 2 ou plus instances en parallèle pour jouer à deux.
- Des screens ou vidéo de la solution fonctionnelle.

Date de rendu : lundi prochain 13 mai.

Temps à y consacrer : 1h30 - 2h.

.. image:: projet_architecture.drawio.svg
  :width: 100%
  :alt: Project architecture