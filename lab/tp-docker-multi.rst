.. sectnum::

TP3 : L'Orchestration
======================================

Objectifs
-----------

Découvrir comment on peut faire fonctionner plusieurs containers ensemble pour mettre en production des architectures complexes.


Docker-compose
------------------------

- Suivez le tp suivant : https://github.com/eficode-academy/docker-katas/blob/master/labs/09-multi-container.md


Ensuite
------------

Docker compose permet de lancer simplement plusieurs containers ensemble. L'étape d'après consiste à définir pour chaque container les ressource à allouer, et les stratégies de scaling ...

La référence dans le domaine s'appelle Kubernetes, et est hors du cadre de cet atelier.