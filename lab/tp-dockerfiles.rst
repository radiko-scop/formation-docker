.. sectnum::

TP2 : Les images Docker
======================================

Objectifs
-----------

- Découvrir les registres d'images docker
- Comprendre comment sont créés les images disponibles


Docker registry
-------------------

Allez sur https://hub.docker.com/, fouinez un peu. Notez deux projets disponibles qui vous paraissent intéressants (pas forcément sérieux).

Vous pouvez vous baser sur la popularité, le nombre de downlaods, chercher des outils que vous utilisez pour le travail ou des choses qui vous amusent (jeux, ...)

Quels sont les images les plus populaires ?
Voyez vous des défaut à ce type de hubs ?

Regardez un peu https://www.docker.com/products/docker-scout/. Comprenez vous son intérêt ?

{{CORR

Correction
**************

En avril 2024 on voyait que les LLMs étaient très populaires, + les basiques pour les applis web : nginx, postgres, ...
Ce type de hub a le défaut à mon avis de fournir beaucoup de packets, mais comme avec tous les packages manager, on a tendance à négliger le suivi des ses dépendances. Il faut tracer les failles et mettre à jour toutes les images dont on dépend, ce qui peut vite être compliqué.
Autre problème : si son auteur décide de stopper le développement, comment sommes nous impactés ?

Exemple : je fait tourner un site avec une image python3.8. Une faille de sécurité rend mon site vulnérable. Comment suis-je supposé le savoir ?

CORR}}


Créer sa propre image Docker
-----------------------------

- Suivre le TP https://github.com/eficode-academy/docker-katas/blob/master/labs/07-building-an-image.md

- Mettez quelques screenshots dans votre compte rendu (au moins votre application en fonctionnement, et l'étape de création de l'image à partir du docker file.)

- Voyez vous des similitudes avec les fichiers de CI qu'on a vu dernièrement ? Lesquelles ?



Une image docker plus complexe
--------------------------------

- Suivre le TP https://github.com/eficode-academy/docker-katas/blob/master/labs/08-multi-stage-builds.md

.. class::info
    Si le TP ci dessus vous semble trop compliqué, vous pouvez vous aider de https://cours.brosseau.ovh/tp/docker/dockerfile.html, voire le faire à la place.

Vous semble-t-il pertinent d'utiliser ce type d'image ? Pourquoi ?
