# In case rst2pdf is installed in virtualenv source .venv/bin/activate
mkdir -p pdf
cp *.png pdf/

labs=("tp-devcontainers.rst"\
      "tp-dockerfiles.rst"\
      "tp-docker-multi.rst"\
      "projet.rst"\
      "tp-docker_cli.rst")

for labPath in "${labs[@]}"
do
	echo $labPath
	filename=$(basename -- "$labPath")
	extension="${filename##*.}"
	stem="${filename%.*}"
	sed '/{{CORR/,/CORR}}/d' $labPath > pdf/${stem}_subject.rst
	rst2pdf pdf/${stem}_subject.rst -s style.yaml --output=pdf/${stem}.pdf

	sed -z 's/{{CORR\|CORR}}//g' $labPath > pdf/${stem}_correction.rst
	rst2pdf pdf/${stem}_correction.rst -s style.yaml --output=pdf/${stem}_correction.pdf
done
