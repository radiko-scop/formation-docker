.. sectnum::

TP1 : Premiers pas avec Docker
======================================

Modalités
-----------

- Un CR par personne. Format libre (papier, .txt, .doc ...) tant que c'est lisible. Des captures d'écrans sont attendues.

Objectifs
-----------

- Se familiariser avec les principes de Docker
- Connaitre les commandes de bases de Docker
- Comprendre les cas dans lesquels Docker est utile


.. class::info

  On parle ici de docker, mais il s'agit d'un outil parmis d'autre pour créer et utiliser des **conteneurs**.
  Docker est le plus populaire, mais il y a des concurrents et des alternatives. On peut citer:

    - Podman (RedHat)
    - lxc

Autres resources
-------------------

- https://cours.brosseau.ovh/tp/docker/introduction.html
- https://docs.docker.com/engine/

Première étape : Installer docker
------------------------------------

Installez Docker desktop sur votre machine si vous ne l'avez pas déjà : `téléchargement ici <https://www.docker.com/products/docker-desktop/>`_

On peut aussi essayer https://container-desktop.io/ comme alternative à Docker desktop (open source)


Premières commandes
-----------------------------------

Commandes de base
+++++++++++++++++++++++++++++++

Pour voir les containers docker en cours d'exécution, lancez la commande ``docker ps`` dans un terminal. Que voyez vous ? Pourquoi ?
Pour voir les containers docker existants, lancez la commande ``docker ps -a`` dans un terminal. Que voyez vous ? Pourquoi ?
Pour voir les images docker existants, lancez la commande ``docker images`` ou ``docker image ls`` dans un terminal. Que voyez vous ? Pourquoi ?

.. class::info
  Vous pouvez voir l'équivallent avec Docker Desktop (le gui). Dans la pratique, Docker compose n'est pas forcément installé sur les
  serveurs et il est plus simple de connaitre les lignes de commande.

{{CORR

Correction
**************

Si vous partez d'une installation vide, tout devrait être vide. Docker n'a rien installé.

CORR}}


Hello world
+++++++++++++++

`Lien de la source <https://github.com/eficode-academy/docker-katas/blob/master/labs/01-hello-world.md>`_.

Lancez la commande ``docker run -it hello-world``.

Expliquez cette commande (``docker run --help`` peut aider).

Est-ce conforme à ce que vous attendiez ?

Relancez les trois commandes de la question précédente. Qu'est ce qui a changé ?

Relancer la commande ``docker run hello-world``.
Est-différent ? Pourquoi ?

{{CORR

Correction
**************

On demande à docker de lancer (run) un container basé sur l'image *hello-world*.

L'option -i permet d'intéragir avec le container (STDIN), donc de taper des commandes.
Ici cela ne sert à rien, le container stoppe avant qu'on puisse taper quoi que ce soit.

L'option -t permet d'afficher ce que le container ainsi lancé écrit dans la sortie standard,
avec son propre formattage.

Lorsqu'on relance les trois commandes précédentes :

- Une image *hello-world* est apparue, qui sert de base pour lancer des containers
- Un container *hello-world* est également apparu et marqué comme stoppé. Cf ligne du dessous.
- Pas de container en cours d'exécution, car *hello-world* retourne rapidement (il n'est pas prévu pour tourner en fond)

.. class::info
  Docker stocke tous les containers et les images utilisées, sauf contre-ordre.
  Ca devient rapidement un problème si l'on ne fait pas attention à nettoyer les caches régulièrement.

Lorsqu'on relance la commande docker run, il n'a plus besoin de télécharger l'image de base, qui est en cache (cf ``docker images``).

CORR}}



Utiliser une image de son choix
+++++++++++++++++++++++++++++++++

`Lien de la source : <https://github.com/eficode-academy/docker-katas/blob/master/labs/02-running-images.md>`_.

Lancez la commande ``docker pull alpine``.

Ca vous rapelle quelque chose ?

Executez ``docker images`` (ou ``docker image ls``).

Que voyez vous ?

Exécutez ``docker run alpine ls -l``. Un container basé sur l'image alpine va se lancer et afficher le résultat de a commande ``ls -l``. Ca vous parait conforme ?

Exécutez ``docker run alpine echo "coucou"``

Exécutez ``docker run alpine /bin/sh``. Un shell devrait se lancer. Qu'observez vous ? Pourquoi ?

Exécutez ``docker run -it alpine /bin/sh``. Le shell se lance bien cette fois (rappelez vous le sens de -it expliqué plus tôt).

Vous devriez maintenant avoir une invite de commande dans le container.
Essayez ``ls -l``.
Essayez ``uname -a``. Notez la valeur obtenue. Est-ce conforme à ce ue vous attendiez ?
Essayez ``cat /proc/cpuinfo`` ``cat /proc/meminfo`` et / ou . Combien de processeurs avez vous ? Qu'en concluez vous ?
Terminez l'exécution dans votre container en tapant ``exit`` dans votre shell alpine.

{{CORR

Correction
**************

- Comme avec la plupart des packages manager, on fait un équivallent de pip - npm - ... install et magie, on obtient une image.
- On voit qu'une nouvelle image est disponible.
- On voit une système de fichier linux basique.
- Le shell retourne direct det le container se ferme, car on ne lui passe pas de terminal, donc il exécute la commande qu'on lui donne et retourne. Ici pas de commande, donc retourne direct.
- Notre container a accès à toutes les resources de a machine, on ne lui a pas alloué qu'une partie du cpu et de la ram comme dans une VM.

CORR}}

Gestion des containers
+++++++++++++++++++++++++++++++++

`Lien de la source <https://github.com/eficode-academy/docker-katas/blob/master/labs/03-deletion.md>`_.


Si vous vous rappelez, on a des *images*, et des *containers*. ``docker image`` permet de manipuler les images. Voici quelques commandes pour manipuler les containers.

Lister les Containers
************************

- Exécutez ``docker ps``. Que voyez vous ? êtes vous surpris ? (``docker ps --help`` pour obtenir de l'aide ...)
- Exécutez ``docker ps -a``. Mieux ? On peut relancer un container avec la fonction ``docker start -i <container id>``.

.. class::info
  Notez que les containers ont un id et un nom généré aléatoirement. On peut imposer le nom d'un container avec l'argument --name, pour mieux s'y retrouver.

Supprimer un container ou une image
**************************************

- Démarrez un nouveau container alpine (``docker run -ti alpine``)
- Vérifiez que vous êtes dans le shell du container, puis :
  - ``ls /``
  - ``whoami`` (Notez qui vous êtes ?)
  - ``date``
- A ce stade, vous voyez que vous avez accès à un certain nombre d'éxécutables compris dans la distribution alpine.
- Vérifiez bien que vous êtes dans votre shell docker, puis faites ``rm -rf /bin``.
- Essayez à nouveau les commandes précédentes. Que se passe il ? Pourquoi ?

Pour remettre les choses en place, c'est simple, il suffit de relancer un container !
Quittez votre container deffectueux (``Ctrl+d``) et recréez une instance docker (``docker run -it alpine``). Avez vous retrouvé vos fonctions ?

.. class::info
  On voit donc que même si quelque chose se passe mal, docker permet de relancer rapidement une nouvelle instance fonctionnelle.
  Attention cependant : si on veut retrouver des données dans le container qu'on a cassé, ce sera un peu plus compliqué ...

- Exécutez la commande ``docker container ls -as``

Vous verrez alors la taille utilisée par vos containers. Un conteneur éteind ne prend pas de place si il est basé sur une image, mais dès qu'il va diverger de son image d'origine en manipulant des fichiers par exemple, elle va grossir.

Si vous créez beaucoup de containers pour tester quelque chose par exemple, vous pouvez demander au dameon de supprimer automatiquement vos container après leur exécution avec la commande ``--rm`` option: ``docker run --rm -it alpine``.

Faites le ménage dans vos container avec la commande ``docker container rm <nom ou id du container>``. Docker vous renvoie le nom du container supprimé.

Vérifiez que cela a bien fonctionné.

On peut supprimer de la même manière les images avec la fonction ``docker image rm <nom ou id>``


Nettoyage
**************

Docker fournit la commande prune pour supprimer automatiquement un certain nombre d'éléments inutilisés:

- ``docker container prune``
- ``docker image prune``
- ``docker network prune``
- ``docker volume prune``
- ``docker system prune``

On peut ajouter l'option ``-a`` pour nettoyer égaement les resources inutilisées.

{{CORR

Correction
**************

Docker ps liste les containers en cours d'exécution. Ici, tous sont terminés, puisqu'on a quitté notre shell.


CORR}}


Lancer un serveur web basique
+++++++++++++++++++++++++++++++++

Avant de commencer, exécutez:

- ``docker run --rm -it alpine``
- Dans le shell ouvert par docker :
  - ``ping google.fr``. Qu'en concluez vous ?
  - ``ip addr``. Quelle adresse avez vous ? Est-elle cohérente avec celle de votre hôte ? Qu'en concluez vous ?

Pour plus de détails, voir https://docs.docker.com/network/ ou bien https://spacelift.io/blog/docker-networking

- Suivez le tp `à cette adresse <https://github.com/eficode-academy/docker-katas/blob/master/labs/04-port-forward.md>`_

Vous allez y apprendre à:
  - Forwarder un port depuis votre machine vers un container.
  - Lancer un container en tâche de fond

{{CORR

Correction
**************

- Docker vous donne accès à internet
- Docker vous donne une adresse, mais qui n'a rien à voir avec votre hôte. Il y a donc ici une couche d'abstraction ou de sécurité. Vous avez accès au réseau via docker.

CORR}}


Aller voir ce qui se passe dans un containeur en cours d'exécution
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- Suivez `le tp suivant <https://github.com/eficode-academy/docker-katas/blob/master/labs/05-executing.md>`_

Prenez un screen shot de votre page que vous mettrez dans votre compte rendu.

Garder des données persistentes
+++++++++++++++++++++++++++++++++

On l'a vu, un aspect intéressant des containers est qu'ils ne stockent aucune donnée persistentes: on veut pouvoir les recréer et les lancer à la volée, et obtenir un container dans le même état qu'avant (stateless ?).
Mais il y a des choses qu'on veut potentiellement garder / partager entre containers.C'est ce qu'on va voir ici.

.. class::info

  Pour plus de détails : https://docs.docker.com/storage/

- Suivez `le tp suivant <https://github.com/eficode-academy/docker-katas/blob/master/labs/06-volumes.md>`_
