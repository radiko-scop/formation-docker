.. sectnum::

TP4 : Docker pour les développeurs avec les devs containers.
================================================================

Objectifs
-----------

Jusquà maintenant on a plutôt vu l'intérêt de docker pour mettre en production des sites ou des micro services.
Dans cette partie, on regarde une application intéressante des containers pour les développeurs, afin de garantir que chaque développeur travaille avec le même environnement de développement.


Introduction : un cas d'usage simple
---------------------------------------

Imaginons que vous avez un projet, et que vous voulez permettre à un collègue de le compiler.

Proposez unse solution basée sur docker pour le faire.


Une généralisation de l'introduction avec les dev containers
-----------------------------------------------------------------

Suivez le tutoriel https://code.visualstudio.com/docs/devcontainers/tutorial

Prenez des screenshots aux moments importants.

Proposer un début de fichier devcontainer.json applicable à votre travail ?

.. class::info
    Ce tuto se base sur une autre fonctionalité de vscode et autre ides : la possibilité de développer à distance (par ex directement sur un raspberry pi).
    Ca peut être très utile quand on ne veut pas ou ne peut pas réinstaller tout un environnement de développement locallement.

Plus d'infos :

- `Sur le standard  <https://containers.dev>`_
- `La doc de vscode sur le sujet <https://code.visualstudio.com/docs/devcontainers/containers>`_